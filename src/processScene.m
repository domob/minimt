##  Copyright (C) 2019-2020  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## processScene (GLTFNAME, PRESETS, SIDE, ZOFFSET, PARCEL)
##
## Processes a full scene.  The output is written as glTF model to a directory
## of the given GLTFNAME.  PRESETS must be a cell matrix with strings,
## describing in what arrangement the preset mountains are loaded and
## put next to each other.
##
## SIDE, ZOFFSET and PARCEL must be scalars.  SIDE determines the real side
## length of the area around each mountain, ZOFFSET is the height in metres
## above ground where the mountains will be placed and PARCEL is the
## side length to which the model will be scaled (corresponding to SIDE).

function processScene (gltfName, presets, side, zoffset, parcel)
  if (!ischar (gltfName))
    print_usage ();
  endif
  if (!isscalar (side) || !isscalar (zoffset) || !isscalar (parcel))
    print_usage ();
  endif

  % Start by loading all the preset data and filling in the combined
  % elevation grid of the scene.
  dists = [];
  eleScene = [];
  for row = 1 : size (presets, 1)
    eleRow = [];
    for col = 1 : size (presets, 2)
      [ele, dist] = loadPreset (presets{row, col});
      ele = extend (dist, ele, side);
      dists = [dists; dist];
      eleRow = [eleRow, ele];
    endfor
    eleScene = [eleScene; eleRow];
  endfor

  % Compute a mean grid distance vector.
  dist = mean (dists, 1);
  dists -= repmat (dist, size (dists, 1), 1);
  relErr = norm (dists, 2, "rows") / norm (dist, 2);
  if (any (relErr > 1e-2))
    error ("Grid spacings are too different for the scene components");
  endif

  % Apply the zoffset.
  eleScene -= min (eleScene(:));
  eleScene += zoffset;

  % Remember where we had real input data.
  fromData = !isna (eleScene);

  % Fill in the missing parts.
  eleScene = fillSpace (dist, eleScene);

  % Compute the normal vectors and reshape them to a matrix (with all
  % vertices in one long list).  This is the format we need later
  % when we work with logical indices to select vertices.
  normv = surfaceNormals (dist, eleScene);
  normv = reshape (normv, [length(eleScene(:)), 3]);

  % Scale to the final target size.
  sz = [size(presets, 2), size(presets, 1)];
  [eleScene, dist] = scale (dist, eleScene, parcel * sz);

  % Build the x/y grid for the scene.
  [xx, yy] = centeredGrid (dist, eleScene);

  % Construct and write voxel model.
  palInd = ones (size (eleScene)) * 0xE7;
  palInd(fromData) = 0xF9;
  xySize = [128, 128];
  [vox, zSize] = voxelise (xx, yy, eleScene, palInd, xySize);
  printf ("We have %d voxels\n", size (vox, 2));
  writeVox (sprintf ("%s.vox", gltfName), [xySize, zSize], vox);

  % Reduce fineness of the grid where we have filled in stuff.  There the
  % surface is smooth anyway, and we can save triangles.
  reducePt = zeros (size (eleScene));
  reducePt(2 : 3 : end - 1) = 1;
  reducePt(3 : 3 : end - 1) = 1;
  keep = (reducePt == 0 | fromData);

  xxRed = xx(keep);
  yyRed = yy(keep);
  eleSceneRed = eleScene(keep);
  fromDataRed = fromData(keep);
  normvRed = normv(keep, :);

  % Build the triangle mesh and preview it.
  tri = delaunay (xxRed, yyRed);
  trimesh (tri, xxRed, yyRed, eleSceneRed);
  xlabel ("Longitude");
  ylabel ("Latitude");

  % Determine the material type (1 for filled in data, 2 for real data).
  types = all (fromDataRed(tri), 2) + 1;
  assert (size (types), [size(tri, 1), 1]);

  % Write the glTF output.
  materials = [0, 0.6, 0, 0, 0.5; ...
               0.8, 0.8, 0.8, 0, 1];
  b = GlTFBuilder (gltfName, materials);
  tri(:, 4) = 1;
  addNode (b, "minimt", [xxRed, yyRed, eleSceneRed], normvRed, tri, types);

  % Build the collider mesh by coarsening the grid again (but much more than
  % we did before).
  inCollider = false (size (eleScene));
  inCollider(1 : 20 : end, 1 : 20 : end) = true;
  inCollider(1 : 20 : end, end) = true;
  inCollider(end, 1 : 20 : end) = true;
  inCollider(end, end) = true;
  xxCol = xx(inCollider(:));
  yyCol = yy(inCollider(:));
  eleSceneCol = eleScene(inCollider(:));
  normvCol = normv(inCollider(:), :);
  triCol = delaunay (xxCol, yyCol);
  triCol(:, 4) = -1;
  types = NA (length (triCol), 1);
  addNode (b, "minimt_collider", [xxCol, yyCol, eleSceneCol], ...
           normvCol, triCol, types);

  finish (b);
endfunction
