##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## ELE = extend (D, ELE, SIDE)
##
## Extends the elevation grid to have the (approximate) side SIDE x SIDE
## (in metres).  The existing grid will be placed in the middle of the
## extended region.

function res = extend (d, ele, side)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif
  if (!isscalar (side))
    print_usage ();
  endif

  % Compute the desired grid size that would match the target.  Note that
  % we swap the components of d, so that neededSz matches the order
  % of size (in rows/columns rather than x/y).
  neededSz = round (side ./ d(2 : -1 : 1)) + 1;

  % The offset (for centering the existing grid in the extended one)
  % is approximately half the extension.
  extension = neededSz - size (ele);
  offset = round (extension / 2);

  if (any (extension < 0))
    error ("Target side for extend is smaller than the existing grid");
  endif

  res = NA (neededSz);
  res(1 + offset(1) : end - (extension(1) - offset(1)), ...
      1 + offset(2) : end - (extension(2) - offset(2))) = ele;
endfunction

%!error <Invalid call> extend (1, rand (4, 4), 1);
%!error <Invalid call> extend ([1, 2, 3], rand (4, 4), 1);
%!error <Invalid call> extend ([1, 2], rand (4, 4), [1, 2]);

%!error <Target side for extend is smaller> extend ([1, 1], rand (4, 4), 2);

%!assert (extend ([1, 1], [1, 2; 3, 4], 1), [1, 2; 3, 4]);
%!assert (extend ([2, 2], [1, 2; 3, 4], 2), [1, 2; 3, 4]);
%!assert (extend ([1, 1], [1, 2; 3, 4], 2), [NA, NA, NA; NA, 1, 2; NA, 3, 4]);
%!assert (extend ([1, 1], [1, 2; 3, 4], 3), ...
%!        [NA, NA, NA, NA; NA, 1, 2, NA; NA, 3, 4, NA; NA, NA, NA, NA]);
