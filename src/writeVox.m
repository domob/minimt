##  Copyright (C) 2020  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## writeVox (FILE, SZ, VOX)
##
## Write a MagicaVoxel .vox file with a single model and the given size and
## voxel data.  VOX should be a matrix with four rows, where the columns
## contain the x, y, z and palette index data for the voxels.

function writeVox (file, sz, vox)
  if (!ischar (file))
    print_usage ();
  endif

  if (!isvector (sz) || length (sz) != 3)
    print_usage ();
  endif

  if (size (vox, 1) != 4)
    print_usage ();
  endif

  cmd = sprintf ("python3 createVox.py %s", file);
  f = popen (cmd, "w");

  fprintf (f, "%d %d %d\n", sz);
  fprintf (f, "%d %d %d %d\n", vox);

  pclose (f);
endfunction
