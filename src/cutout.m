##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## ELE = cutout (D, ELE, R, LVL)
##
## "Cuts out" an area around the centre of the given elevation matrix.
## All entries that are further than R from the middle point are set to NA,
## as are all areas whose absolute elevation is below LVL.

function ele = cutout (d, ele, r, lvl)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif
  if (!isscalar (r) || !isscalar (lvl))
    print_usage ();
  endif

  [xx, yy] = centeredGrid (d, ele);
  ele(xx.^2 + yy.^2 > r^2) = NA;
  ele(ele < lvl) = NA;
endfunction

%!error <Invalid call> cutout (1, rand (4, 4), 1, 2);
%!error <Invalid call> cutout ([1, 2, 3], rand (4, 4), 1, 2);
%!error <Invalid call> cutout ([1, 2], rand (4, 4), [1, 2], 2);
%!error <Invalid call> cutout ([1, 2], rand (4, 4), 1, [2, 3]);

%!assert (cutout (ones (1, 2), [1, 2; 3, 4], 10, 2.5), [NA, NA; 3, 4]);
%!assert (cutout (ones (1, 2), [1, 2, 3; 4, 5, 6; 7, 8, 9], 1.1, 0), ...
%!        [NA, 2, NA; 4, 5, 6; NA, 8, NA]);
