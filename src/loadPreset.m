##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [ELE, D] = loadPreset (NAME)
##
## Loads the elevation grid corresponding to a named preset mountain.

function [ele, d] = loadPreset (name)
  if (!ischar (name))
    print_usage ();
  endif

  switch (name)
    case "matterhorn"
      r = 700;
      lvl = 3500;
      pos = [45.97645, 7.65857];

    case "rigi"
      r = 700;
      lvl = 1400;
      pos = [47.0567213, 8.4853246];

    case "tamischbachturm"
      r = 800;
      lvl = 1400;
      pos = [47.61489, 14.69909];

    case "lugauer"
      r = 700;
      lvl = 1800;
      pos = [47.5531381, 14.7223489];

    case "hochtor"
      r = 700;
      lvl = 1800;
      pos = [47.5618943, 14.6325770];

    otherwise
      error ("Unknown preset mountain: %s", name);
  endswitch

  [ele, d] = loadMountain (pos, r, lvl);
endfunction
