##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [ELE, D] = loadMountain (LATLNG, R, LVL)
##
## Loads an area around the given latitude/longitude coordinate.  The loaded
## part will be cut out in a circular fashion (with the given radius R) and
## all parts with elevation LVL will be cut as well.  Those points in the
## returned elevation grid will be set to NA.

function [ele, d] = loadMountain (latlng, r, lvl, side)
  if (!isvector (latlng) || length (latlng) != 2)
    print_usage ();
  endif
  if (!isscalar (r) || !isscalar (lvl))
    print_usage ();
  endif

  [ele, d] = loadAround (latlng, r);
  ele = cutout (d, ele, r, lvl);
endfunction

%!error <Invalid call> loadMountain (1, 2, 3);
%!error <Invalid call> loadMountain ([1, 2, 3], 2, 3);
%!error <Invalid call> loadMountain ([1, 2], [2, 3], 3);
%!error <Invalid call> loadMountain ([1, 2], 2, [3, 4]);
