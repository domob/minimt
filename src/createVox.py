#!/usr/bin/env python3

#   Mini Mountains, constructing 3D models from elevation data.
#   Copyright (C) 2020  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

# A simple Python script that reads a basic text description of a voxel model
# from stdin and uses py-vox-io to then serialise that as MagicaVoxel file.

from pyvox.models import (Vox, Size, Voxel, Model)
from pyvox.writer import VoxWriter

import sys

if len (sys.argv) != 2:
  sys.exit ("USAGE: python3 createVox.py OUTFILE")
outfile = sys.argv[1]

sz = [int (n) for n in sys.stdin.readline ().split (" ")]
assert len (sz) == 3

voxels = []
for l in sys.stdin:
  numbers = [int (n) for n in l.split (" ")]
  assert len (numbers) == 4
  voxels.append (Voxel (*numbers))

vox = Vox ([Model (Size (*sz), voxels)])
VoxWriter (outfile, vox).write ()
