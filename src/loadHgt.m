##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## ELE = loadHgt (FILENAME)
##
## Loads the HGT file with the given name.  Returned is the raw elevation data
## as a matrix.  The first row contains the southern-most row of elevations,
## from west to east.  The last row contains the northern-most data.  This
## means that ELE(y, x) contains the elevation data for the given (x, y)
## coordinate (with x being longitude and y latitude).
##
## Missing entries in the data file will be replaced by NA values.

function ele = loadHgt (filename)
  [info, err, msg] = stat (filename);
  if (err < 0)
    error ("Error opening %s: %s", filename, msg);
  endif
  sz = info.size;

  if (mod (sz, 2) != 0)
    error ("File %s has size %d, which is not even", filename, sz);
  endif
  numel = sz / 2;

  n = round (sqrt (numel));
  if (n^2 != numel)
    error ("File %s contains %d entries, which is not square", filename, numel);
  endif

  % The HGT file contains raw data entries in the form of 16-bit integers
  % in big-endian byte order.
  [f, msg] = fopen (filename, "rb", "b");
  if (f < 0)
    error ("Error opening %s: %s", filename, msg);
  endif
  ele = fread (f, numel, "int16");
  fclose (f);

  % The raw HGT file contains the data in row-major order, so we have to
  % transpose the result of reshape.
  ele = reshape (ele, [n, n])';

  % In the HGT file, the first row is northernmost (with largest rather
  % than lowest latitude).  Thus we have to reverse the order of rows.
  ele = ele(end : -1 : 1, :);

  ele(ele == -2^15) = NA;
endfunction
