##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [ELE, D] = scale (D, ELE, SZ)
##
## Scales the elevation grid and vector of grid spacings so that the size
## is approximately as in SZ.  SZ is a vector containing the desired x
## and y dimensions of the grid.
##
## It must be possible to scale the grid uniformly to the target size,
## else an error is returned.

function [ele, d] = scale (d, ele, sz)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif
  if (!isvector (sz) || length (sz) != 2)
    print_usage ();
  endif

  [~, ~, realSize] = centeredGrid (d, ele);
  fact = sz ./ realSize;
  meanFact = mean (fact);

  relErr = abs (fact(1) - fact(2)) / meanFact;
  if (relErr > 1e-2)
    error ("Cannot scale the grid uniformly; relative error: %.2e", relErr);
  endif

  ele *= meanFact;
  d *= meanFact;
endfunction

%!error <Invalid call> scale (1, rand (4, 4), [1, 1]);
%!error <Invalid call> scale ([1, 2, 3], rand (4, 4), [1, 1]);
%!error <Invalid call> scale ([1, 2], rand (4, 4), 1);
%!error <Invalid call> scale ([1, 2], rand (4, 4), [1, 2, 3]);

% For some reason, the regexp for "cannot scale" does not work.
%!error scale ([1, 1], rand (2, 3), [10, 10]);

%!test
%!  [ele, d] = scale ([1, 2], [1, 2; 3, 4; 5, 6], [10, 40]);
%!  assert (d, [10, 20]);
%!  assert (ele, [10, 20; 30, 40; 50, 60]);
