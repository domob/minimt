##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## TRI = orientTriangles (TRI, X, Y, Z)
##
## Modifies a list of triangles (in the N x 3 matrix TRI, as returned e.g.
## from delaunay) such that they are all oriented in the same way.  In
## particular, triangles are reordered such that (b - a) x (c - a) has
## a positive z component for all of them.
##
## X, Y and Z must be vectors of the same length, containing the coordinates
## of the triangle vertices.

function tri = orientTriangles (tri, x, y, z)
  n = size (tri, 1);
  if (size (tri, 2) != 3)
    print_usage ();
  endif
  if (!isvector (x)
        || !isvector (y) || length (y) != length (x)
        || !isvector (z) || length (z) != length (x))
    print_usage ();
  endif

  % Make sure that all vertex vectors are columns.
  if (isrow (x))
    x = x';
  endif
  if (isrow (y))
    y = y';
  endif
  if (isrow (z))
    z = z';
  endif
  assert (iscolumn (x) && iscolumn (y) && iscolumn (z));

  % As the first step, we collect all first, second and third
  % vertices of triangles into xyz vectors.
  a = [x(tri(:, 1)), y(tri(:, 1)), z(tri(:, 1))];
  b = [x(tri(:, 2)), y(tri(:, 2)), z(tri(:, 2))];
  c = [x(tri(:, 3)), y(tri(:, 3)), z(tri(:, 3))];

  % Next, compute the cross products of the sides of each triangle.
  nv = cross (b - a, c - a, 2);

  % Finally, swap first and second vertex of all triangles that are
  % oriented in the wrong direction.
  wrong = (nv(:, 3) < 0);
  tri(wrong, 1 : 2) = tri(wrong, 2 : -1 : 1);
endfunction

%!error <Invalid call> orientTriangles (ones (10, 2), 1, 2, 3);
%!error <Invalid call> orientTriangles (ones (10, 3), rand (2, 2), 2, 2);
%!error <Invalid call> orientTriangles (ones (10, 3), 1, 2, [3, 3]);

%!test
%!  x = [0, 1, 0];
%!  y = [0, 0, 1];
%!  z = [0, 1, 2];
%!  tri = [1, 2, 3; 2, 3, 1; 3, 1, 2; 2, 1, 3];
%!  assert (orientTriangles (tri, x, y, z), ...
%!          [1, 2, 3; 2, 3, 1; 3, 1, 2; 1, 2, 3]);
