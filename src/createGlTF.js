/*
    Mini Mountains, constructing 3D models from elevation data.
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* This is a simple Node.js script, which just reads some triangle data from
   stdin and uses gltf-js-utils to produce a glTF file from it.  This is used
   internally from createGlTF.m.  */

window = {};
const GLTFUtils = require ("gltf-js-utils");
const fs = require ("fs");

/**
 * Parses the given material specifications from strings and returns an
 * array of material instances for glTF utils.
 */
function ParseMaterialSpecs (specs)
{
  const res = [];
  for (var i in specs)
    {
      const parts = specs[i].split (",");
      if (parts.length != 5)
        {
          console.log ("Invalid material specification: " + specs[i]);
          return [];
        }
      const parsed = [];
      for (var j in parts)
        parsed.push (parseFloat (parts[j]));

      const m = new GLTFUtils.Material ();
      m.doubleSided = true;
      m.pbrMetallicRoughness.baseColorFactor
          = [parsed[0], parsed[1], parsed[2], 0];
      m.pbrMetallicRoughness.metallicFactor = parseFloat (parts[3]);
      m.pbrMetallicRoughness.roughnessFactor = parseFloat (parts[4]);
      res.push (m);
    }

  return res;
}

/**
 * Constructs a Node instance from the data that is returned "line by line"
 * from the reader function.
 */
function ConstructNode (readLine)
{
  const res = new GLTFUtils.Node ();
  res.mesh = new GLTFUtils.Mesh ();

  const vertices = [];
  const numVertices = parseInt (readLine ());
  console.log ("Parsing " + numVertices + " vertices...");
  for (var i = 0; i < numVertices; ++i)
    {
      const v = new GLTFUtils.Vertex ();
      v.x = parseFloat (readLine ());
      v.y = parseFloat (readLine ());
      v.z = parseFloat (readLine ());
      v.normalX = parseFloat (readLine ());
      v.normalY = parseFloat (readLine ());
      v.normalZ = parseFloat (readLine ());
      vertices.push (v);
    }

  /* We have to add the triangles of each material together, else we build up
     many different primitives in our model.  Thus we first collect all
     triangles into arrays by material, and only later add them to the actual
     glTF model with GLTFUtils.  */
  const trianglesByPart = {};

  const numTriangles = parseInt (readLine ());
  console.log ("Adding " + numTriangles + " triangles...");
  for (var i = 0; i < numTriangles; ++i)
    {
      const v1 = parseInt (readLine ()) - 1;
      const v2 = parseInt (readLine ()) - 1;
      const v3 = parseInt (readLine ()) - 1;
      const part = parseInt (readLine ());

      if (!(part in trianglesByPart))
        trianglesByPart[part] = [];

      const cur =
        {
          vert: [vertices[v1], vertices[v2], vertices[v3]],
          mat: (part == 0 ? undefined : part - 1),
        };
      trianglesByPart[part].push (cur);
    }

  for (p in trianglesByPart)
    {
      const cur = trianglesByPart[p];
      console.log ("Part " + p + " has " + cur.length + " triangles");
      for (i in cur)
        {
          const t = cur[i];
          res.mesh.addFace (t.vert[0], t.vert[1], t.vert[2], null, t.mat);
        }
    }

  return res;
}

/**
 * Constructs a line-reader closure for the given file.
 */
function CreateFileReader (filename)
{
  const lines = fs.readFileSync (filename, "ascii").split ("\n");
  /* Due to the split'ing, there will actually be a "spurious" last line
     consisting of an empty string.  We need to take that into account.  */

  var ln = 0;
  function readLine ()
    {
      if (ln < lines.length - 1)
        return lines[ln++];
      return null;
    }

  return readLine;
}

/* The body of the script follows below.  */

if (process.argv.length < 3)
  {
    console.log ("USAGE: node createGlTF.js OUTDIR MATERIALS...");
    return;
  }

const outdir = process.argv[2];
const materialSpecs = process.argv.splice (3);

const dataReader = CreateFileReader ("/dev/stdin");

const asset = new GLTFUtils.GLTFAsset ();
const scene = new GLTFUtils.Scene ();
asset.addScene (scene);

while (true)
  {
    const nodeName = dataReader ();
    if (nodeName == null)
      break;
    console.log ("Next model: " + nodeName);

    const node = ConstructNode (dataReader);
    node.name = nodeName;
    node.mesh.material = ParseMaterialSpecs (materialSpecs);
    scene.addNode (node);
  }

GLTFUtils.exportGLTF (asset).then (function (fileData)
  {
    for (name in fileData)
      {
        console.log ("Writing file: " + name);
        fs.writeFile (outdir + "/" + name,
                      new Buffer (fileData[name]),
                      function (err)
          {
            if (err)
              console.log (err);
          });
      }
  });
