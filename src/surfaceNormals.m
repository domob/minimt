##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## N = surfaceNormals (D, ELE)
##
## Computes the unit normal vector for the surface given by the graph of the
## ELE function.  D must be a vector with two elements, giving the grid
## spacing in x- and y-direction.
##
## Returned is an R x C x 3 array, where R x C is the dimension of ELE.  For
## each point in ELE, R(i, j, :) gives the normal vector in that point.

function n = surfaceNormals (d, ele)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif

  % The first step is computing the x and y derivatives in all the points
  % of our matrix.  For this, we use central differences.  To easily handle
  % the boundary, we simply extend the matrix by a row of zeros temporarily
  % (we know that we have homogeneous Dirichlet boundary conditions!).
  padded = zeros (size (ele) + 2);
  padded(2 : end - 1, 2 : end - 1) = ele;

  % Now we can compute the derivatives.
  dx = padded(2 : end - 1, 3 : end) - padded(2 : end - 1, 1 : end - 2);
  dx /= (2 * d(1));
  dy = padded(3 : end, 2 : end - 1) - padded(1 : end - 2, 2 : end - 1);
  dy /= (2 * d(2));

  % From the derivatives, we can find the tangent vectors to the surface
  % in each point and direction.
  tx = [ones(size (dx(:))), zeros(size (dx(:))), dx(:)];
  ty = [zeros(size (dy(:))), ones(size (dy(:))), dy(:)];

  % Finally, the normal vector is given by the cross product of the two
  % tangent vectors.
  n = cross (tx, ty, 2);
  n ./= norm (n, 2, "rows");
  n = reshape (n, [size(ele), 3]);
endfunction

%!error <Invalid call> surfaceNormals (2, rand (2, 2));
%!error <Invalid call> surfaceNormals (rand (2, 2), rand (2, 2));

% Basic test where we have an all-zero function.
%!test
%!  res = NA (5, 5, 3);
%!  res(:, :, 1) = 0;
%!  res(:, :, 2) = 0;
%!  res(:, :, 3) = 1;
%!  assert (surfaceNormals ([1, 1], zeros (5, 5)), res);

%!shared n, m, x, y, d, xx, yy, res
%!  n = 13;
%!  m = 13;
%!  x = linspace (-2, 2, n + 2);
%!  x = x(2 : end - 1);
%!  y = linspace (-2, 2, m + 2);
%!  y = y(2 : end - 1);
%!  d = [(x(2) - x(1)), (y(2) - y(1))];
%!  [xx, yy] = meshgrid (x, y);
%!  assert (size (xx), [m, n]);
%!  assert (size (yy), size (xx));
%!  res = NA (m, n, 3);
%!  assert (size (res(:, :, 1)), size (xx));
%!endfunction

% Test a function with known x derivative and no y dependence.
%!test
%!  ele = 4 - xx.^2;
%!  alpha = atan (-2 * x);
%!  res(:, :, 1) = -ones (m, 1) * sin (alpha);
%!  res(:, :, 2) = 0;
%!  res(:, :, 3) = ones (m, 1) * cos (alpha);
%!  normv = surfaceNormals (d, ele);
%!
%!  % The top and bottom rows are wrong due to the assumed zero
%!  % boundary conditions for y.  Thus we have to ignore them.
%!  assert (normv(2 : end - 1, :, :), res(2 : end - 1, :, :), -1e-12);

% Test a function with known y derivative and no x dependence.
%!test
%!  ele = 4 - yy.^2;
%!  alpha = atan (-2 * y);
%!  res(:, :, 1) = 0;
%!  res(:, :, 2) = -sin (alpha') * ones (1, n);
%!  res(:, :, 3) = cos (alpha') * ones (1, n);
%!  normv = surfaceNormals (d, ele);
%!
%!  % The left and right columns are wrong due to the assumed zero
%!  % boundary conditions for x.  Thus we have to ignore them.
%!  assert (normv(:, 2 : end - 1, :), res(:, 2 : end - 1, :), -1e-12);

% Test a function with x and y dependence.
%!test
%!  ele = 4 - xx.^2 - yy.^2;
%!  radius = sqrt (xx.^2 + yy.^2);
%!  alphaRadial = atan (-2 * radius);
%!  normv = surfaceNormals (d, ele);
%!
%!  % The z coordinate of the right normal can be determined easily, just
%!  % from the radius of a point.  As before, we need to exclude the outer
%!  % layer of points due to the boundary conditions.
%!  nz = cos (alphaRadial);
%!  assert (normv(2 : end - 1, 2 : end - 1, 3), ...
%!          nz(2 : end - 1, 2 : end - 1), -1e-12);
%!
%!  % The x and y coordinates are given by a magnitude depending on the
%!  % radius, and the direction of the point itself.
%!  magXY = -sin (alphaRadial);
%!  dir = NA (m, n, 2);
%!  dir(:, :, 1) = magXY .* xx ./ radius;
%!  dir(:, :, 2) = magXY .* yy ./ radius;
%!  mid = (n + 1) / 2;
%!  dir(mid, mid, :) = 0;
%!  assert (normv(2 : end - 1, 2 : end - 1, 1 : 2), ...
%!          dir(2 : end - 1, 2 : end - 1, :), -1e-12);
