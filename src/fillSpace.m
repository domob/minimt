##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## ELE = fillSpace (D, ELE)
##
## Fills in all NA elements of ELE in such a way that the resulting surface
## is smooth and continuous.  Towards the outer border, it will go down
## continuously to zero.
##
## D must be a vector with two elements, giving the spacing between grid
## points in the x- and y-directions.

function ele = fillSpace (d, ele)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif

  % After solving the smoothing system, we need to add a layer of real zeros
  % around to reach the final elevation matrix (since the equation system
  % has the zeros implicit for "outside the range").  Thus we require that
  % at least one row of NAs is all around the matrix, so that we can fill in
  % the zeros there.
  if (!all (isna (ele(1, :))) || !all (isna (ele(end, :))) ...
        || !all (isna (ele(:, 1))) || !all (isna (ele(:, end))))
    error ("elevation matrix must have NA all around");
  endif
  inner = ele(2 : end - 1, 2 : end - 1);

  [M, f, vm] = fillSystem (d, inner);
  fills = M \ f;
  missing = isna (inner);
  inner(missing) = fills(vm(missing));

  ele = zeros (size (ele));
  ele(2 : end - 1, 2 : end - 1) = inner;
endfunction

%!error <Invalid call> fillSpace (2, rand (2, 2));
%!error <Invalid call> fillSpace (rand (2, 2), rand (2, 2));

%!error <NA all around>
%!  ele = NA (3, 3);
%!  ele(1, 2) = 5;
%!  fillSpace ([1, 1], ele);

%!error <NA all around>
%!  ele = NA (3, 3);
%!  ele(2, 1) = 5;
%!  fillSpace ([1, 1], ele);

%!error <NA all around>
%!  ele = NA (3, 3);
%!  ele(3, 2) = 5;
%!  fillSpace ([1, 1], ele);

%!error <NA all around>
%!  ele = NA (3, 3);
%!  ele(2, 3) = 5;
%!  fillSpace ([1, 1], ele);

%!test
%!  ele = NA (50, 50);
%!  middle = rand (30, 30);
%!  ele(11 : 40, 11 : 40) = middle;
%!  ele = fillSpace ([1, 1], ele);
%!
%!  assert (ele(1, :), zeros (1, 50));
%!  assert (ele(end, :), zeros (1, 50));
%!  assert (ele(:, 1), zeros (50, 1));
%!  assert (ele(:, end), zeros (50, 1));
%!  assert (ele(11 : 40, 11 : 40), middle);
