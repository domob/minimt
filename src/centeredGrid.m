##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [XX, YY, SZ] = centeredGrid (D, ELE)
##
## Returns x and y coordinates of a grid that is placed over the elevation
## matrix.  The origin will be at the middle point of ELE.  x spans the
## longitude (columns of ELE) and y spans the latitude (rows).
##
## The size in x and y direction of the entire grid is returned in the
## vector SZ.

function [xx, yy, realSize] = centeredGrid (d, ele)
  if (!isvector (d) || length (d) != 2)
    print_usage ();
  endif

  sz = size (ele);
  realSize = [d(1)*(sz(2) - 1), d(2)*(sz(1) - 1)];

  x = linspace (-realSize(1) / 2, realSize(1) / 2, sz(2));
  y = linspace (-realSize(2) / 2, realSize(2) / 2, sz(1));

  [xx, yy] = meshgrid (x, y);
endfunction

%!error <Invalid call> centeredGrid (1, rand (4, 4));
%!error <Invalid call> centeredGrid ([1, 2, 3], rand (4, 4));

%!test
%!  [xx, yy, sz] = centeredGrid ([1, 2], zeros (3, 5));
%!  assert (xx, repmat (-2 : 2, 3, 1));
%!  assert (yy, repmat ((-2 : 2 : 2)', 1, 5));
%!  assert (sz, [4, 4]);

%!test
%!  [xx, yy, sz] = centeredGrid ([1, 2], zeros (2, 4));
%!  assert (xx, repmat (-1.5 : 1.5, 2, 1));
%!  assert (yy, repmat ((-1 : 2 : 1)', 1, 4));
%!  assert (sz, [3, 2]);
