##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## D = arcsec (LAT)
##
## Computes the real distance (in metres) corresponding to an arc second
## at the given latitude coordinate.  Returned is a two-element vector,
## containing the x and y distances for an arc second.  x corresponds
## to the longitude and y to the latitude.

function d = arcsec (lat)
  if (!isscalar (lat))
    print_usage ();
  endif

  rEarth = 5371e3;
  yDist = rEarth * (2 * pi) * (1 / 3600 / 360);
  xDist = cosd (lat) * yDist;

  d = [xDist, yDist];
endfunction
