##  Copyright (C) 2019  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [ELE, D] = loadAround (LATLNG, RANGE)
##
## Loads the elevation data around the given (latitude, longitude) coordinate
## and returns it together with a vector describing the real distances
## (in metres) between the grid points in either direction (x = longitude
## and y = latitude).
##
## The data loaded is in a rectangular region around the centre LATLNG, with
## RANGE metres (approximately) in each direction.  The pixel corresponding to
## the centre coordinate will be at the exact centre of the returned matrix,
## and the size will be odd in both directions (so there is an exact centre).
## The data in the matrix is ordered such that the first row contains the
## northern-most row from west to east.
##
## This function only works if the entire range is contained in one of the
## data tiles, and the data tile for the given centre point is found in
## the ../data folder.

function [ele, d] = loadAround (latlng, range)
  if (!isvector (latlng) || length (latlng) != 2)
    print_usage ();
  endif
  if (!isscalar (range))
    print_usage ();
  endif

  tile = floor (latlng);
  filename = sprintf ("../data/N%02dE%03d.hgt", tile(1), tile(2));

  ele = loadHgt (filename);
  d = arcsec (latlng(1));

  n = size (ele, 1);
  assert (size (ele, 2), n);

  secPerPixel = 3600 / (n - 1);
  d *= secPerPixel;

  ind = round ((latlng - tile) * 3600 / secPerPixel);
  pxRange = round (range ./ d);
  if (any (ind - pxRange < 1) || any (ind + pxRange > n))
    error ("Centre coordinate is too close to boundary of tile");
  endif

  ele = ele(ind(1) - pxRange(2) : ind(1) + pxRange(2), ...
            ind(2) - pxRange(1) : ind(2) + pxRange(1));
endfunction
