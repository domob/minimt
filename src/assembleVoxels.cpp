/*
    Copyright (C) 2020  Daniel Kraft <d@domob.eu>
    Mini Mountains, constructing 3D models from elevation data.
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
  
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <octave/oct.h>

#include <vector>

namespace
{

/**
 * Data for one voxel, so we can store it conveniently in a vector
 * and do some basic stuff with it.
 */
struct Voxel
{

  int x, y, z, pal;

  explicit Voxel (const int xx, const int yy, const int p)
    : x(xx), y(yy), z(0), pal(p)
  {}

  Voxel (const Voxel&) = default;
  Voxel& operator= (const Voxel&) = default;

  Voxel
  ForZ (const int zz) const
  {
    Voxel res(*this);
    res.z = zz;
    return res;
  }

};

/**
 * Given the z grid and an index into it, find the minimum value of
 * neighbouring cells.  Returns -1 if the coordinate is on the border.
 */
int
MinimumNeighbourValue (const Matrix& z, const int i, const int j)
{
  const auto dims = z.dims ();

  if (i == 0 || j == 0)
    return -1;

  if (i == dims(0) - 1 || j == dims(1) - 1)
    return -1;

  return std::min (std::min (z(i, j - 1), z(i, j + 1)),
                   std::min (z(i - 1, j), z(i + 1, j)));
}

} // anonymous namespace

DEFUN_DLD (assembleVoxels, args, nargout,
   "VOX = assembleVoxels (XX, YY, ZZ, PAL)\n\n"

   "Assembles a final voxel model (i.e. the list of voxels) from the\n"
   "quantised surface.\n\n"

   "XX, YY, ZZ and PAL should be matrices that give the x, y, z indices (in\n"
   "terms of voxel quants) as well as palette indices for the surface.\n\n"

   "The returned VOX is a 4-row matrix containing all the voxels that make\n"
   "up the assembled voxel model.")
{
  if (args.length () != 4 || nargout > 1)
    {
      print_usage ();
      return octave_value_list ();
    }

  const Matrix xx = args(0).matrix_value ();
  const auto dims = xx.dims ();
  assert (dims.length () == 2);

  const Matrix yy = args(1).matrix_value ();
  if (yy.dims () != dims)
    {
      error ("input matrices size mismatch");
      return octave_value_list ();
    }

  const Matrix zz = args(2).matrix_value ();
  if (zz.dims () != dims)
    {
      error ("input matrices size mismatch");
      return octave_value_list ();
    }

  const Matrix pal = args(3).matrix_value ();
  if (pal.dims () != dims)
    {
      error ("input matrices size mismatch");
      return octave_value_list ();
    }

  std::vector<Voxel> voxels;
  for (unsigned i = 0; i < dims(0); ++i)
    for (unsigned j = 0; j < dims(1); ++j)
      {
        Voxel v(xx(i, j), yy(i, j), pal(i, j));

        /* We build a hollow voxel model, where we leave out all internal
           voxels that are invisible from the outside anyway.  For this, we
           have to include the bottom one at z=0 and the top one at the
           actual z value, obviously.  But also all voxels for z values below
           the maximum, down to one more than the minimum z of all neighbour
           cells.  For entries on the border of the grid, we have to include
           all voxels down to zero.  */

        voxels.push_back (v);
        if (zz(i, j) == 0)
          continue;
        voxels.push_back (v.ForZ (zz(i, j)));

        const int minVal = MinimumNeighbourValue (zz, i, j);
        for (unsigned k = minVal + 1; k < zz(i, j); ++k)
          voxels.push_back (v.ForZ (k));
      }

  Matrix vox(4, voxels.size ());
  for (unsigned i = 0; i < voxels.size (); ++i)
    {
      vox(0, i) = voxels[i].x;
      vox(1, i) = voxels[i].y;
      vox(2, i) = voxels[i].z;
      vox(3, i) = voxels[i].pal;
    }

  octave_value_list res;
  res(0) = vox;

  return res;
}
