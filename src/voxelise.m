##  Copyright (C) 2020  Daniel Kraft <d@domob.eu>
##  Mini Mountains, constructing 3D models from elevation data.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU Affero General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU Affero General Public License for more details.
##
##  You should have received a copy of the GNU Affero General Public License
##  along with this program.  If not, see <https://www.gnu.org/licenses/>.

## [VOX, ZSIZE] = voxelise (XX, YY, ELE, PALIND, XYSIZE)
##
## Computes the voxels representing an elevation grid given by XX, YY and ELE.
## The x and y number of voxels is given in XYSIZE, and the voxels themselves
## as well as the z-size of the voxel grid are returned.  PALIND is used as
## the palette index for voxels at a given position.

function [vox, zSize] = voxelise (xx, yy, ele, palInd, xySize)
  if (any (size (xx) != size (yy)))
    print_usage ();
  endif
  if (any (size (xx) != size (ele)))
    print_usage ();
  endif
  if (any (size (xx) != size (palInd)))
    print_usage ();
  endif

  if (!isvector (xySize) || length (xySize) != 2)
    print_usage ();
  endif

  % We start by shifting all grids to be positive for simplicity.
  xx -= min (xx(:));
  yy -= min (yy(:));
  ele -= min (ele(:));

  % Then compute the maximum extends in each direction.
  xLen = max (xx(:));
  yLen = max (yy(:));
  zLen = max (ele(:));

  % The sizes of a voxel in each direction.
  xVoxSize = xLen / (xySize(1) - 1);
  yVoxSize = yLen / (xySize(2) - 1);
  zVoxSize = mean ([xVoxSize, yVoxSize]);

  % Determine how many voxels we need in z direction.
  zSize = max (floor (ele(:) / zVoxSize)) + 1;

  % Convert to a solid voxel model.
  [ii, jj] = meshgrid (1 : xySize(1), 1 : xySize(2));
  realXX = (ii - 1) * xVoxSize;
  realYY = (jj - 1) * yVoxSize;
  kk = interp2 (xx, yy, ele, realXX, realYY);
  kk = max (0, floor (kk / zVoxSize));
  pal = interp2 (xx, yy, palInd, realXX, realYY, "nearest");
  vox = assembleVoxels (ii, jj, kk, pal);
endfunction
