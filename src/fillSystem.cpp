/*
    Copyright (C) 2019  Daniel Kraft <d@domob.eu>
    Mini Mountains, constructing 3D models from elevation data.
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
  
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <octave/oct.h>

#include <cassert>
#include <cmath>
#include <utility>
#include <vector>

namespace
{

/**
 * Checks whether the given point is free (i.e. ele is NA and it is not
 * out of bounds).
 */
bool
IsFree (const dim_vector& dims, const Matrix& ele,
        const octave_idx_type i, const octave_idx_type j,
        const octave_idx_type di, const octave_idx_type dj)
{
  const octave_idx_type ip = i + di;
  const octave_idx_type jp = j + dj;

  if (ip < 0 || ip >= dims(0) || jp < 0 || jp >= dims(1))
    return false;

  return lo_ieee_is_NA (ele(ip, jp));
}

/**
 * Updates the system by "adding" one particular difference from a centre
 * variable that is free (i.e. to be filled in).
 */
void
AddDifference (const dim_vector& dims, const Matrix& ele, const Matrix& vertmap,
               const double h, const octave_idx_type i, const octave_idx_type j,
               const octave_idx_type di, const octave_idx_type dj,
               double& centre, SparseMatrix& M, double& rhs)
{
  const octave_idx_type ip = i + di;
  const octave_idx_type jp = j + dj;

  const double hinv = 1.0 / std::pow (h, 2);
  centre += hinv;

  /* Apply homogeneous Dirichlet boundary conditions on the outside
     of the rectangular grid.  */
  if (ip < 0 || ip >= dims(0) || jp < 0 || jp >= dims(1))
    return;

  /* Apply inhomogeneous Dirichlet boundary conditions towards the already
     filled in sections.  */
  if (!lo_ieee_is_NA (ele(ip, jp)))
    {
      rhs += hinv * ele(ip, jp);
      return;
    }

  /* Construct standard Laplacian for inner differences.  */
  assert (!lo_ieee_is_NA (vertmap(ip, jp)));
  const octave_idx_type ind = vertmap(i, j) - 1;
  const octave_idx_type indp = vertmap(ip, jp) - 1;
  M(ind, indp) = -hinv;
}

/**
 * Adds the Laplace equation difference that would be needed in one of the
 * fixed points that has free neighbours.  These equations overdetermine
 * the system, and add constraints that lead to continuity also of the
 * derivative across the boundary to the fixed region (and not just the
 * function itself).
 */
void
AddExtraDiff (const dim_vector& dims, const Matrix& ele, const Matrix& vertmap,
              const double h, const octave_idx_type i, const octave_idx_type j,
              const octave_idx_type di, const octave_idx_type dj,
              const octave_idx_type ind, SparseMatrix& M, double& rhs)
{
  const octave_idx_type ip = i + di;
  const octave_idx_type jp = j + dj;
  assert (ip >= 0 && ip < dims(0));
  assert (jp >= 0 && jp < dims(1));

  const double hinv = 1.0 / std::pow (h, 2);

  assert (!lo_ieee_is_NA (ele(i, j)));
  rhs -= hinv * ele(i, j);

  /* If the difference points towards another fixed point, this adds to the
     residual on the right-hand side.  */
  if (!lo_ieee_is_NA (ele(ip, jp)))
    {
      rhs += hinv * ele(ip, jp);
      return;
    }

  /* Otherwise, the equation for the current centre fixed point contains
     a reference to a free variable.  Add the corresponding coefficient
     in the system matrix.  */
  assert (!lo_ieee_is_NA (vertmap(ip, jp)));
  const octave_idx_type indp = vertmap(ip, jp) - 1;
  M(ind, indp) = -hinv;
}

} // anonymous namespace

DEFUN_DLD (fillSystem, args, nargout,
   "[M, F, VM] = fillSystem (D, ELE)\n\n"

   "Builds and returns a finite-difference system (with matrix M and\n"
   "right-hand side vector F) that, when solved, fill in the NA parts\n"
   "of the elevation matrix ELE.  The result will be zero beyond the outer\n"
   "boundary and as smooth as possible towards the fixed regions.\n\n"

   "D must be a vector of two elements, giving the spacing between grid\n"
   "points in the x and y directions.\n\n"

   "Note that the returned system is overdetermined and should be solved\n"
   "by minimising the residual (\\ is fine).\n\n"

   "VM is set to a matrix of the same shape as ELE.  Whenever a pixel is\n"
   "filled in (i.e. ELE is NA), the corresponding pixel will be set to the\n"
   "one-based index of that pixel in the variables of the created equation\n"
   "system.  Points with fixed value in ELE will be NA in VM.")
{
  if (args.length () != 2 || nargout > 3)
    {
      print_usage ();
      return octave_value_list ();
    }

  const ColumnVector d = args(0).column_vector_value ();
  if (d.numel () != 2)
    {
      print_usage ();
      return octave_value_list ();
    }

  const Matrix ele = args(1).matrix_value ();
  const auto dims = ele.dims ();
  assert (dims.length () == 2);

  /* Start by checking which entries we need to fill in at all and populating
     the vertex map with them.  */
  octave_idx_type nextInd = 0;
  Matrix vertmap(dims);
  for (octave_idx_type i = 0; i < dims(0); ++i)
    for (octave_idx_type j = 0; j < dims(1); ++j)
      {
        if (lo_ieee_is_NA (ele(i, j)))
          {
            ++nextInd;
            vertmap(i, j) = nextInd;
          }
        else
          vertmap(i, j) = octave_NA;
      }
  const octave_idx_type numFree = nextInd;

  /* Look for coordinates that have a fixed value but neighbours that are
     free.  For those, we add extra equations that try to minimise the
     Laplace residual also in that points.  */
  std::vector<std::pair<octave_idx_type, octave_idx_type>> extraPts;
  for (octave_idx_type i = 0; i < dims(0); ++i)
    for (octave_idx_type j = 0; j < dims(1); ++j)
      {
        if (lo_ieee_is_NA (ele(i, j)))
          continue;
        if (IsFree (dims, ele, i, j, -1, 0)
              || IsFree (dims, ele, i, j, 1, 0)
              || IsFree (dims, ele, i, j, 0, -1)
              || IsFree (dims, ele, i, j, 0, 1))
          extraPts.emplace_back (i, j);
      }
  const octave_idx_type numExtra = extraPts.size ();

  /* Build up the main FD system matrix and RHS.  */
  SparseMatrix M(numFree + numExtra, numFree, 5 * numFree + 4 * numExtra);
  ColumnVector f(numFree + numExtra);
  for (octave_idx_type i = 0; i < dims(0); ++i)
    for (octave_idx_type j = 0; j < dims(1); ++j)
      {
        if (!lo_ieee_is_NA (ele(i, j)))
          continue;
        
        const octave_idx_type ind = vertmap(i, j) - 1;
        assert (ind >= 0 && ind < numFree);

        double centre = 0.0;
        double rhs = 0.0;

        AddDifference (dims, ele, vertmap, d(0), i, j, 0, -1, centre, M, rhs);
        AddDifference (dims, ele, vertmap, d(0), i, j, 0, 1, centre, M, rhs);
        AddDifference (dims, ele, vertmap, d(1), i, j, -1, 0, centre, M, rhs);
        AddDifference (dims, ele, vertmap, d(1), i, j, 1, 0, centre, M, rhs);

        M(ind, ind) = centre;
        f(ind) = rhs;
      }

  /* Add the extra residual equations.  */
  for (octave_idx_type n = 0; n < numExtra; ++n)
    {
      const octave_idx_type ind = n + numFree;
      const auto i = extraPts[n].first;
      const auto j = extraPts[n].second;

      double rhs = 0.0;

      AddExtraDiff (dims, ele, vertmap, d(0), i, j, 0, -1, ind, M, rhs);
      AddExtraDiff (dims, ele, vertmap, d(0), i, j, 0, 1, ind, M, rhs);
      AddExtraDiff (dims, ele, vertmap, d(1), i, j, -1, 0, ind, M, rhs);
      AddExtraDiff (dims, ele, vertmap, d(1), i, j, 1, 0, ind, M, rhs);

      f(ind) = rhs;
    }

  M.maybe_compress ();

  octave_value_list res;
  res(0) = M;
  res(1) = f;
  res(2) = vertmap;

  return res;
}
