# Data Files

The scripts and tools will look for elevation data files (in HGT format)
in this folder by default.  Each data file should be named
`NddEeee.hgt`, where `dd` is degrees latitude north and `eee` degrees
longitude east of the south-western corner.  Each data file is assumed
to cover a one degree times one degree area.

(For now, the scripts do not support data on the southern or western
hemispheres for simplicity.  They can easily be extended to do so, though.
If you make this change, I'm happy to accept pull requests.)

You can get data files from the [SRTM project](https://www2.jpl.nasa.gov/srtm/),
Jonathan de Ferranti's [Viewfinder](http://viewfinderpanoramas.org/)
or perhaps from other sources.
