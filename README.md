# Mini Mountains: 3D Models of Real Mountains

This project is a collection of scripts and tools that can be used to
build **3D models of real mountains**.  In particular, real elevation data
(e.g. [SRTM data](https://www2.jpl.nasa.gov/srtm/) or the processed
data from Jonathan de Ferranti's
[Viewfinder project](http://viewfinderpanoramas.org/)) is used
as the starting point.
The elevation data for mountains can then be "cut out", assembled to
groups as desired, and the space in between filled with a smooth surface.
The result can be saved as 3D model in
[**glTF format**](https://www.khronos.org/gltf) for further processing.

My motivation for this project is **building scenes for
[Decentraland](https://decentraland.org/)**.  But of course, the models
can also be used for other purposes.
